import 'package:flutter/material.dart';

import 'P14PayrollViewer/PayrollViewer.dart';

class Page14 extends StatelessWidget {
  const Page14({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page14Body();
  }
}

class Page14Body extends StatelessWidget {
  const Page14Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PayrollViewer();
    // return Center(
    //     child: Container(
    //       height: 100,
    //       width: 100,
    //       color: Colors.blue,
    //       child: const Text("PAGE 1"),
    //     ),
    //   );
  }
}
