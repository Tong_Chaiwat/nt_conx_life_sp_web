import 'package:flutter/material.dart';

class CampaignBuilderNew_subP extends StatelessWidget {
  const CampaignBuilderNew_subP({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: 42,
                    height: 42,
                    decoration: BoxDecoration(
                      color: const Color(0xfff4c700),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x3f000000),
                            offset: Offset(0, 1),
                            blurRadius: 2,
                            spreadRadius: 0),
                      ],
                    )),
                //left zone
                Container(
                  padding: const EdgeInsets.only(left: 7, top: 19),
                  width: 590,
                  height: 957,
                  decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      borderRadius: BorderRadius.circular(8)),
                  child: SingleChildScrollView(
                    child: Center(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 550,
                              height: 188,
                              decoration: const BoxDecoration(
                                color: Colors.green,
                                // color: Color(0xffffffff),
                                //   borderRadius: BorderRadius.circular(24),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x1c000000),
                                      offset: Offset(0, 0),
                                      blurRadius: 2,
                                      spreadRadius: 0)
                                ],
                              ),
                              child: Row(
                                children: [
                                  Container(
                                      width: 188,
                                      height: 188,
                                      decoration: const BoxDecoration(
                                        color: Colors.grey,
                                        // color: const Color(0xffffffff),
                                        // borderRadius: BorderRadius.circular(24),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0x1c000000),
                                              offset: Offset(0, 0),
                                              blurRadius: 2,
                                              spreadRadius: 0)
                                        ],
                                      )),
                                  const SizedBox(width: 30),
                                ],
                              ),
                            ),
                            const SizedBox(height: 46),
                            const Text("ชื่อหัวข้อสินค้า/บริการ",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet',
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(height: 15),
                            Container(
                                width: 550,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  //   color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(4),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                            const SizedBox(height: 19),
                            const Text("รายละเอียดสินค้า/บริการ",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet',
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(height: 15),
                            Container(
                                width: 550,
                                height: 238,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  //   color: Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(4),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                            const SizedBox(height: 15),
                            const Text("URL",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet',
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(height: 19),
                            Container(
                                width: 550,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  //   color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(4),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                            const SizedBox(height: 19), //จำนวนของ Voucher
                            const Text("จำนวนของ Voucher",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet',
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(height: 15),
                            Container(
                                width: 145,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  //   color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(4),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                            //----------------------------------------------------------------

                            //
                          ]),
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 35),
                      child: Container(
                          width: 42,
                          height: 148,
                          decoration: BoxDecoration(
                              color: const Color(0xfff4c700),
                              borderRadius: BorderRadius.circular(8))),
                    ),
                    Container(
                      width: 458,
                      height: 956,
                      padding: const EdgeInsets.only(
                          top: 15, left: 15, right: 15, bottom: 15),
                      decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                        borderRadius: BorderRadius.circular(24),
                        border: Border.all(
                          color: Colors
                              .black, //                   <--- border color
                          width: 2.0,
                        ),
                      ),
                      child: Container(
                        height: 956 - 15,
                        width: 458 - 15 - 15,
                        decoration: BoxDecoration(
                            color: const Color(0x7f000000),
                            borderRadius: BorderRadius.circular(24)),
                        child: Center(
                          child: Column(
                            children: [
                              const SizedBox(height: 90),
                              Container(
                                height: 56,
                                width: 56,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.orange,
                                    shape: BoxShape.circle),
                                child: const Icon(
                                  size: 30,
                                  Icons.close,
                                ),
                              ),
                              const SizedBox(height: 15),
                              Container(
                                width: 458 - 15 - 15,
                                height: 761,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                  ),
                                  color: Colors.blue,
                                ),
                                child: Center(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        const SizedBox(
                                          height: 84,
                                        ),
                                        Container(
                                          width: 188,
                                          height: 188,
                                          decoration: BoxDecoration(
                                            color: const Color(0xffffffff),
                                            borderRadius:
                                                BorderRadius.circular(24),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color(0x1c000000),
                                                  offset: Offset(0, 0),
                                                  blurRadius: 2,
                                                  spreadRadius: 0)
                                            ],
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 18,
                                        ),
                                        const Text(
                                          "[โปรดระบุชื่อหัวข้อสินค้า / บริการ โดยมีเนื้อหาไม่เกิน 2 บรรทัด หรือ 64 ตัวอักษร]",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff070504),
                                            fontSize: 22,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(22),
                                          child: const Text(
                                              "[โปรดระบุรายละเอียดของสินค้าและการบริการ และเงื่อนไขต่างๆ ที่สำคัญต่อการใช้งาน Voucher นี้ เนื้อหาควรไม่เกิน 1 กระดาษ A4 หรือ 2048 ตัวอักษร]",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff070504),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing:
                                                    -0.320000001362392,
                                              )),
                                        ),
                                        Container(
                                            //height: 215,
                                            padding:
                                                const EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                ListTile(
                                                  leading: Container(
                                                      color: Colors.red,
                                                      height: 24,
                                                      width: 24),
                                                  title: const Text(
                                                      "URL อธิบายรายละเอียดเพิ่มเติม",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff2b80bf),
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: 0,
                                                      )),
                                                ),
                                                ListTile(
                                                  leading: Container(
                                                      color: Colors.red,
                                                      height: 24,
                                                      width: 24),
                                                  title: const Text(
                                                      "เบอร์ติดต่อ (ถ้ามี) สำหรับผู้ที่สนใจ",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff2b80bf),
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: 0,
                                                      )),
                                                ),
                                                ListTile(
                                                  leading: Container(
                                                      color: Colors.red,
                                                      height: 24,
                                                      width: 24),
                                                  title: const Text(
                                                      "จำนวน Voucher ที่สร้างไว้ - Voucher ที่ใช้ไปแล้ว",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff2b80bf),
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: 0,
                                                      )),
                                                ),
                                                ListTile(
                                                  leading: Container(
                                                      color: Colors.red,
                                                      height: 24,
                                                      width: 24),
                                                  title: const Text(
                                                      "วันสิ้นสุด Campaign",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff2b80bf),
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: 0,
                                                      )),
                                                ),
                                                ListTile(
                                                  leading: Container(
                                                      color: Colors.red,
                                                      height: 24,
                                                      width: 24),
                                                  title: const Text(
                                                      "ระยะโดยประมาณ คิดจาก ผู้ใช้งาน -> ร้านค้า",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff2b80bf),
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: 0,
                                                      )),
                                                )
                                              ],
                                            )),
                                        const SizedBox(
                                          height: 30,
                                        ),
                                        Container(
                                            width: 382,
                                            height: 49,
                                            decoration: BoxDecoration(
                                              color: const Color(0xfff4c700),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color(0x3f000000),
                                                    offset: Offset(0, 1),
                                                    blurRadius: 2,
                                                    spreadRadius: 0)
                                              ],
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ]),
        ),
      ),
    );
  }
}
