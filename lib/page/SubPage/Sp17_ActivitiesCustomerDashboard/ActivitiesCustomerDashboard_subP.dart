import 'package:flutter/material.dart';

class ActivitiesCustomerDashboard_subP extends StatelessWidget {
  const ActivitiesCustomerDashboard_subP({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          //Zone 0<Top>
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.center,
                color: Colors.green,
                height: 80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 550,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const Text("รายงานสถิติ",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet',
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            Container(
                                width: 104,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Color(0xfff4c700),
                                    borderRadius: BorderRadius.circular(20))),
                            Container(
                                width: 104,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Color(0xfff4c700),
                                    borderRadius: BorderRadius.circular(20))),
                            Container(
                                width: 104,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Color(0xfff4c700),
                                    borderRadius: BorderRadius.circular(20)))
                          ]),
                    ),
                    Container(
                      width: 171,
                      height: 42,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: const Color(0xfff4c700),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: const [
                          BoxShadow(
                              color: Color(0x3f000000),
                              offset: Offset(0, 1),
                              blurRadius: 2,
                              spreadRadius: 0)
                        ],
                      ),
                      child: const Text("Staff  Logs",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff000000),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                    )
                  ],
                )),
          ),
          const SizedBox(
            height: 20,
          ),
          //Zone 1<Bottom>
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              color: Colors.blue,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //Zone 1_0<Bottom>
                    Container(
                      width: 380,
                      height: 829,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: [
                          //Zone 1_00<Bottom>
                          Container(
                            padding: const EdgeInsets.all(20.0),
                            height: 283,
                            color: Colors.brown,
                            child: Stack(children: [
                              const Align(
                                alignment: Alignment.topLeft,
                                child: Text("รายได้วันนี้",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ),
                              const Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  "อัพเดทล่าสุด 01/11/2022 09:15",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xffffffff),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  ),
                                ),
                              ),
                              const Align(
                                alignment: Alignment(-0.4, 0),
                                child: Text("123K",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 64,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              const Align(
                                alignment: Alignment(0.6, 0.15),
                                child: Text("บาท",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              )
                            ]),
                          ),
                          //Zone 1_01<Bottom>

                          Container(
                            height: 829 - 283,
                            color: Colors.green.shade300,
                            padding: EdgeInsets.all(20),
                            width: 380,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("รายการที่ทำรายได้มากที่สุด",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff000000),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                const SizedBox(
                                  height: 40,
                                ),
                                const Center(
                                  child: SizedBox(
                                    height: 217,
                                    width: 217,
                                    child: CircularProgressIndicator(
                                      value: 0.5,
                                      semanticsLabel:
                                          'Circular progress indicator',
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 170,
                                ),
                                Center(
                                  child: Container(
                                    width: 260,
                                    height: 42,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff4c700),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ),
                                    child: const Text("ดูสถิติรายได้สะสม",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        )),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    //Zone 1_1<Bottom>
                    Container(
                      width: 380,
                      height: 829,
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(8)),
                      child: Column(
                        children: [
                          //Zone 1_10<Bottom>
                          Container(
                            padding: const EdgeInsets.all(20.0),
                            height: 283,
                            color: Color(0xfff83600),
                            child: Stack(children: [
                              const Align(
                                alignment: Alignment.topLeft,
                                child: Text("Vouchers ที่ถูกใช้งานวันนี้",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ),
                              const Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  "อัพเดทล่าสุด 01/11/2022 09:15",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xffffffff),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  ),
                                ),
                              ),
                              const Align(
                                alignment: Alignment(-0.4, 0),
                                child: Text("56K",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 64,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              const Align(
                                alignment: Alignment(0.6, 0.15),
                                child: Text("คน",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              )
                            ]),
                          ),
                          //Zone 1_01<Bottom>

                          Container(
                            height: 829 - 283,
                            color: Colors.green.shade300,
                            padding: EdgeInsets.all(20),
                            width: 380,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("รายการที่มีคนสนใจมากที่สุด",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff000000),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                const SizedBox(
                                  height: 40,
                                ),
                                const Center(
                                  child: SizedBox(
                                    height: 217,
                                    width: 217,
                                    child: CircularProgressIndicator(
                                      value: 0.5,
                                      semanticsLabel:
                                          'Circular progress indicator',
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 170,
                                ),
                                Center(
                                  child: Container(
                                    width: 260,
                                    height: 42,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff4c700),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ),
                                    child: const Text("ดูสถิติความนิยมสะสม",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        )),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    //Zone 1_2<Bottom>
                    Container(
                      width: 380,
                      height: 829,
                      decoration: BoxDecoration(
                        color: Colors.blueGrey,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: [
                          //Zone 1_20<Bottom>
                          Container(
                            padding: const EdgeInsets.all(20.0),
                            height: 283,
                            color: Color(0xfff2af598),
                            child: Stack(children: [
                              const Align(
                                alignment: Alignment.topLeft,
                                child: Text("ลูกค้าใหม่",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                    width: 42,
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ),
                              const Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  "อัพเดทล่าสุด 01/11/2022 09:15",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xffffffff),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  ),
                                ),
                              ),
                              const Align(
                                alignment: Alignment(-0.4, 0),
                                child: Text("+32",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 64,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              const Align(
                                alignment: Alignment(0.6, 0.15),
                                child: Text("คน",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xffffffff),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              )
                            ]),
                          ),
                          //Zone 1_01<Bottom>

                          Container(
                            height: 829 - 283,
                            color: Colors.green.shade300,
                            padding: EdgeInsets.all(20),
                            width: 380,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Text("Customers Activities Logs",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff000000),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                const SizedBox(
                                  height: 35,
                                ),
                                SizedBox(
                                  height: 370,
                                  width: 380,
                                  // child: CircularProgressIndicator(
                                  //   value: 0.5,
                                  //   semanticsLabel:
                                  //       'Circular progress indicator',
                                  // ),
                                  child: ListView.separated(
                                    itemCount: 25,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            Divider(height: 1),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return ListTile(
                                        title: Text(
                                            'NT008E69 ใช้ Voucher 500 Happo Credits… $index'),
                                      );
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  height: 25,
                                ),
                                Container(
                                  width: 260,
                                  height: 42,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: const Color(0xfff4c700),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: const Text("ดูสถิติความนิยมสะสม",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff000000),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
