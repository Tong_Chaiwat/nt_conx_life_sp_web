import 'package:flutter/material.dart';

class IncomeChart_subP extends StatelessWidget {
  const IncomeChart_subP({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Center(
            child: Column(
          children: [
            Container(
              height: 80,
              padding: EdgeInsets.symmetric(horizontal: 30),
              decoration: BoxDecoration(
                color: Color(0xfff4c700),
                borderRadius: BorderRadius.circular(18),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 550,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                color: const Color(0xffffffff),
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color(0x3f000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2,
                                      spreadRadius: 0)
                                ],
                              )),
                          const Text("สถิติรายได้สะสม",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                color: const Color(0xffffffff),
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color(0x3f000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2,
                                      spreadRadius: 0)
                                ],
                              )),
                          const Text("อัพเดทล่าสุด\n01/11/2022 09:15",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff000000),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ))
                        ]),
                  ),
                  SizedBox(
                    // width: 320,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          const Text("Campaign",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          SizedBox(
                            width: 16,
                          ),
                          Container(
                              width: 251,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Color(0xffffffff),
                                borderRadius: BorderRadius.circular(4),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x3f000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2,
                                      spreadRadius: 0),
                                ],
                              )),
                          SizedBox(
                            width: 16,
                          ),
                          Text("ปี",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          SizedBox(
                            width: 16,
                          ),
                          Container(
                              width: 106,
                              height: 40,
                              decoration: new BoxDecoration(
                                color: Color(0xffffffff),
                                borderRadius: BorderRadius.circular(4),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x3f000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2,
                                      spreadRadius: 0)
                                ],
                              ))
                        ]),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            //________________________________________________________________
            Container(
              // width: 1227,
              alignment: Alignment.center,
              height: 742,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Container(
                  width: 1010,
                  height: 668,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: const Color(0xffe8e8e8),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text('Chart')),
            ),
//________________________________________________________________
            Container(
              height: 80,
              alignment: Alignment.center,
              // decoration: BoxDecoration(
              //   color: Color(0xfff4c700),
              //   borderRadius: BorderRadius.circular(18),
              // ),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const Text("Download:",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff000000),
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                const SizedBox(
                  width: 20,
                ),
                Container(
                    width: 97.09794437726725,
                    height: 40.78113663845227,
                    decoration: BoxDecoration(
                        color: const Color(0xfff4c700),
                        borderRadius: BorderRadius.circular(8))),
                const SizedBox(
                  width: 20,
                ),
                Container(
                    width: 97.09794437726725,
                    height: 40.78113663845227,
                    decoration: BoxDecoration(
                        color: const Color(0xfff4c700),
                        borderRadius: BorderRadius.circular(8))),
              ]),
            ),
          ],
        )),
      ),
    );
  }
}
