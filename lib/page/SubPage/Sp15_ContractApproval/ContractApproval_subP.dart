import 'package:flutter/material.dart';

class ContractApproval_subP extends StatelessWidget {
  const ContractApproval_subP({super.key});

  @override
  Widget build(BuildContext context) {
    Color _color(int selection) {
      int filter = selection % 2;
      if (filter == 0) {
        return Color(0xffe5e5e5);
      } else {
        return Color(0xffffffff);
      }
    }

    final List<String> dataTest = [
      'NT008E69',
      '500 Happo Credits for Mental Health Analysis',
      'ยกเลิก',
      'XMC83222ABS533',
      '0',
      '01/11/2022',
      '11:22',
    ];
    List<Text> headerDataText = [
      const Text(
        "Customer ID",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "Campaign Voucher",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "กิจกรรม",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "Voucher ID",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "THB",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "วันที่ (วว/ดด/ปป)",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      const Text(
        "เวลา (ชช:นน)",
        style: TextStyle(
          fontFamily: 'SukhumvitSet',
          color: Color(0xff000000),
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
    ];
    return Center(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 27),
      child: Column(
        children: [
          const SizedBox(
            height: 40,
          ),
          //Zone 0
          Container(
            // color: Colors.pink,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //Zone 01<left>
                Container(
                  //  color: Colors.green,
                  height: 50,
                  // width: 300,
                  child: Row(children: [
                    Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                          color: const Color(0xfff4c700),
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0, 1),
                                blurRadius: 2,
                                spreadRadius: 0)
                          ],
                        )),
                    const SizedBox(
                      width: 24,
                    ),
                    const Text("Contract",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet',
                          color: Color(0xff000000),
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        )),
                    const SizedBox(
                      width: 24,
                    ),
                    Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                          color: const Color(0xfff4c700),
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0, 1),
                                blurRadius: 2,
                                spreadRadius: 0)
                          ],
                        )),
                    const SizedBox(
                      width: 17,
                    ),
                    const Text("อัพเดทล่าสุด\n01/11/2022 09:15",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff000000),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ))
                  ]),
                ),
                //Zone 01<Right>
                Container(
                  // color: Colors.yellow,
                  height: 50,
                  child: Row(
                    children: [
                      const Text("Nicety Nine Co., Ltd.",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff000000),
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      const SizedBox(
                        width: 17,
                      ),
                      Container(
                          width: 67,
                          height: 67,
                          decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(4)))
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Zone 1
          const SizedBox(
            height: 11,
          ),
          Expanded(
            child: //Zone 1<Bottom>
                Padding(
              padding: const EdgeInsets.all(14.0),
              child: Container(
                color: Colors.blue,
                height: 842,
                child: SingleChildScrollView(
                  child: Column(children: [
                    const SizedBox(
                      height: 27,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 354,
                          height: 40,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 1),
                                  blurRadius: 2,
                                  spreadRadius: 0)
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 27,
                        ),
                        Container(
                            width: 101,
                            height: 42,
                            decoration: BoxDecoration(
                              color: const Color(0xfff4c700),
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color(0x3f000000),
                                    offset: Offset(0, 1),
                                    blurRadius: 2,
                                    spreadRadius: 0)
                              ],
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 27,
                    ),
                    //header Data
                    Container(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            Container(
                              width: 162,
                              height: 40,
                              alignment: Alignment.center,
                              color: Colors.amberAccent,
                              child: headerDataText[0],
                            ),
                            Container(
                              width: 344,
                              height: 40,
                              alignment: Alignment.centerLeft,
                              color: Colors.red,
                              child: headerDataText[1],
                            ),
                            Container(
                              width: 148,
                              height: 40,
                              alignment: Alignment.centerLeft,
                              color: Colors.grey,
                              child: headerDataText[2],
                            ),
                            Container(
                              width: 170,
                              height: 40,
                              alignment: Alignment.center,
                              color: Colors.amberAccent,
                              child: headerDataText[3],
                            ),
                            Container(
                              width: 60,
                              height: 40,
                              alignment: Alignment.center,
                              color: Colors.pink,
                              child: headerDataText[4],
                            ),
                            Container(
                              width: 170,
                              height: 40,
                              alignment: Alignment.center,
                              color: Colors.cyan,
                              child: headerDataText[5],
                            ),
                            Container(
                              width: 170,
                              height: 40,
                              alignment: Alignment.center,
                              color: Colors.cyan,
                              child: headerDataText[6],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //data
                    Container(
                      width: 1224, //เอาความกว้างของ header Data มารวมกัน
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            width: 1224, //เอาความกว้างของ header Data มารวมกัน
                            height: 623,
                            child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {},
                                  onHover: (bool hover) {
                                    if (hover) {
                                      print(index);
                                    }
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 162,
                                        height: 40,
                                        alignment: Alignment.center,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[0],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 344,
                                        height: 40,
                                        alignment: Alignment.centerLeft,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[1],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 220 - 72,
                                        height: 40,
                                        alignment: Alignment.centerLeft,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[2],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 170,
                                        height: 40,
                                        alignment: Alignment.center,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[3],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 170 - 110,
                                        height: 40,
                                        alignment: Alignment.center,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[4],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 170,
                                        height: 40,
                                        alignment: Alignment.center,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[5],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 170,
                                        height: 40,
                                        alignment: Alignment.center,
                                        color: _color(index),
                                        child: Text(
                                          dataTest[6],
                                          style: const TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          )),
                    )
                  ]),
                ),
              ),
            ),
            // child: SingleChildScrollView(
            //   child: Container(
            //     padding: EdgeInsets.all(10),
            //     //  width: 1202,
            //     height: 842,
            //     decoration: BoxDecoration(
            //         color: const Color(0xffffffff),
            //         // color: Colors.amberAccent,
            //         borderRadius: BorderRadius.circular(8)),
            //     child: Column(children: [
            //       const SizedBox(height: 36),
            // Row(
            //   children: [
            //     Container(
            //         width: 354,
            //         height: 40,
            //         decoration: BoxDecoration(
            //           color: const Color(0xffffffff),
            //           borderRadius: BorderRadius.circular(4),
            //           boxShadow: const [
            //             BoxShadow(
            //                 color: Color(0x3f000000),
            //                 offset: Offset(0, 1),
            //                 blurRadius: 2,
            //                 spreadRadius: 0)
            //           ],
            //         )),
            //     const SizedBox(width: 20),
            //     Container(
            //         width: 101,
            //         height: 42,
            //         decoration: BoxDecoration(
            //           color: const Color(0xfff4c700),
            //           borderRadius: BorderRadius.circular(8),
            //           boxShadow: const [
            //             BoxShadow(
            //                 color: Color(0x3f000000),
            //                 offset: Offset(0, 1),
            //                 blurRadius: 2,
            //                 spreadRadius: 0)
            //           ],
            //         ))
            //   ],
            // ),

            //     ]),
            //   ),
            // ),
          ),
        ],
      ),
    ));
  }
}
