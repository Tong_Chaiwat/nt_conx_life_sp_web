import 'package:flutter/material.dart';

class CampaignBuilderEdit_supP extends StatelessWidget {
  const CampaignBuilderEdit_supP({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> _text = ['1000', '100', '0'];
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Center(
        child: SingleChildScrollView(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Zone00
              Container(
                  width: 42,
                  height: 42,
                  decoration: BoxDecoration(
                    color: const Color(0xfff4c700),
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: const [
                      BoxShadow(
                          color: Color(0x3f000000),
                          offset: Offset(0, 1),
                          blurRadius: 2,
                          spreadRadius: 0)
                    ],
                  )),

              // Zone01
              Container(
                width: 500,
                //height: 965,
                //    color: Colors.pink,
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: 42,
                          height: 148,
                          decoration: new BoxDecoration(
                              color: const Color(0xfff4c700),
                              borderRadius: BorderRadius.circular(8))),
                      Container(
                        width: 458,
                        height: 956,
                        padding: const EdgeInsets.only(
                            top: 15, left: 15, right: 15, bottom: 15),
                        decoration: BoxDecoration(
                          color: const Color(0xffffffff),
                          borderRadius: BorderRadius.circular(24),
                          border: Border.all(
                            color: Colors
                                .black, //                   <--- border color
                            width: 2.0,
                          ),
                        ),
                        child: Container(
                          height: 956 - 15,
                          width: 458 - 15 - 15,
                          decoration: BoxDecoration(
                              color: const Color(0x7f000000),
                              borderRadius: BorderRadius.circular(24)),
                          child: Center(
                            child: Column(
                              children: [
                                const SizedBox(height: 90),
                                Container(
                                  height: 56,
                                  width: 56,
                                  alignment: Alignment.center,
                                  decoration: const BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle),
                                  child: const Icon(
                                    size: 30,
                                    Icons.close,
                                  ),
                                ),
                                const SizedBox(height: 15),
                                Container(
                                  width: 458 - 15 - 15,
                                  height: 761,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20),
                                    ),
                                    color: Colors.blue,
                                  ),
                                  child: Center(
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          const SizedBox(
                                            height: 84,
                                          ),
                                          Container(
                                            width: 188,
                                            height: 188,
                                            decoration: BoxDecoration(
                                              color: const Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(24),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color(0x1c000000),
                                                    offset: Offset(0, 0),
                                                    blurRadius: 2,
                                                    spreadRadius: 0)
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 18,
                                          ),
                                          const Text(
                                            "5,000 Happo Credits for\nMental Health Analysis",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet',
                                              color: Color(0xff070504),
                                              fontSize: 22,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 24,
                                          ),
                                          Container(
                                            padding: const EdgeInsets.all(22),
                                            child: const Text(
                                                "Analyzing any possibilities of the internalising  disorders of the users and recommend the mental exercises program to improve their mental health altogether.",
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SukhumvitSet-Text',
                                                  color: Color(0xff070504),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing:
                                                      -0.320000001362392,
                                                )),
                                          ),
                                          Container(
                                              height: 215,
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  ListTile(
                                                    leading: Container(
                                                        color: Colors.red,
                                                        height: 24,
                                                        width: 24),
                                                    title: const Text(
                                                        "https://happo.app",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'SukhumvitSet-Text',
                                                          color:
                                                              Color(0xff2b80bf),
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0,
                                                        )),
                                                  ),
                                                  ListTile(
                                                    leading: Container(
                                                        color: Colors.red,
                                                        height: 24,
                                                        width: 24),
                                                    title: const Text(
                                                        "0628899942 (Campaign Admin)",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'SukhumvitSet-Text',
                                                          color:
                                                              Color(0xff2b80bf),
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0,
                                                        )),
                                                  ),
                                                  ListTile(
                                                    leading: Container(
                                                        color: Colors.red,
                                                        height: 24,
                                                        width: 24),
                                                    title: const Text(
                                                        "คงเหลือ 1,000",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'SukhumvitSet-Text',
                                                          color:
                                                              Color(0xff2b80bf),
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0,
                                                        )),
                                                  ),
                                                  ListTile(
                                                    leading: Container(
                                                        color: Colors.red,
                                                        height: 24,
                                                        width: 24),
                                                    title: const Text("Online",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'SukhumvitSet-Text',
                                                          color:
                                                              Color(0xff2b80bf),
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0,
                                                        )),
                                                  )
                                                ],
                                              )),
                                          const SizedBox(
                                            height: 30,
                                          ),
                                          Container(
                                            width: 382,
                                            height: 49,
                                            decoration: BoxDecoration(
                                              color: const Color(0xfff4c700),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color(0x3f000000),
                                                    offset: Offset(0, 1),
                                                    blurRadius: 2,
                                                    spreadRadius: 0)
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
              //
              // Zone02
              Container(
                  width: 257,
                  height: 956,
                  color: Colors.green,
                  padding: EdgeInsets.symmetric(vertical: 100),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Zone02_00

                      Center(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 64,
                                height: 64,
                                decoration: BoxDecoration(
                                  color: const Color(0xfff4c700),
                                  borderRadius: BorderRadius.circular(4),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 2),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 25,
                              ),
                              SizedBox(
                                child: Column(children: const [
                                  Text("Unpin",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet',
                                        color: Color(0xff000000),
                                        fontSize: 24,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  Text("[Status: Pinned]",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff070504),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.240000001021794,
                                      ))
                                ]),
                              )
                            ]),
                      ),
                      // Zone02_01
                      Container(
                        height: 164,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 64,
                                    height: 64,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff4c700),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 2),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 25,
                                  ),
                                  SizedBox(
                                    child: Column(children: const [
                                      Text("Active",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 24,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          )),
                                      // Text("[Status: Pinned]",
                                      //     style: TextStyle(
                                      //       fontFamily: 'SukhumvitSet-Text',
                                      //       color: Color(0xff070504),
                                      //       fontSize: 12,
                                      //       fontWeight: FontWeight.w400,
                                      //       fontStyle: FontStyle.normal,
                                      //       letterSpacing: -0.240000001021794,
                                      //     ))
                                    ]),
                                  )
                                ]),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 64,
                                    height: 64,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff4c700),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 2),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 25,
                                  ),
                                  SizedBox(
                                    child: Column(children: const [
                                      Text("Archive",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff000000),
                                            fontSize: 24,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0,
                                          )),
                                      // Text("[Status: Pinned]",
                                      //     style: TextStyle(
                                      //       fontFamily: 'SukhumvitSet-Text',
                                      //       color: Color(0xff070504),
                                      //       fontSize: 12,
                                      //       fontWeight: FontWeight.w400,
                                      //       fontStyle: FontStyle.normal,
                                      //       letterSpacing: -0.240000001021794,
                                      //     ))
                                    ]),
                                  )
                                ]),
                          ],
                        ),
                      )
                    ],
                  )),
              // Zone03
              Container(
                width: 380,
                height: 956,
                color: Colors.purple,
                child: Column(
                  children: [
                    // Zone03_00
                    Container(
                      width: 380,
                      height: 383,
                      decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    const SizedBox(height: 18),
                    // Zone03_01
                    Container(
                      width: 380,
                      height: 459,
                      decoration: BoxDecoration(
                        color: const Color(0xfff4c700),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: [
                          for (var item in _text)
                            Container(
                              width: 380,
                              height: 120,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: item == '1000'
                                      ? const Color(0xff09b4e3)
                                      : item == '100'
                                          ? const Color(0xffb33928)
                                          : const Color(0xff14b181)),
                              child: Text('$item'),
                            )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
