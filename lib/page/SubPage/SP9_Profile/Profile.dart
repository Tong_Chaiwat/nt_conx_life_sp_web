import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    const double heightCollum_ZoneB = 1000.0;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        child: Container(
          child: Row(children: [
            Expanded(
              //*CenterLeft<---ZoneB0->
              child: Container(
                // color: Colors.pink,
                height: heightCollum_ZoneB, //700,
                child: Column(children: [
                  //!<---ZoneB00->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    // color: const Color(0xffff884f),
                    height: 244, // 700,
                    child: Row(
                      children: [
                        Container(
                          height: 244,
                          width: 244,
                          alignment: Alignment.centerLeft,
                          color: const Color(0xff7b7b7b),
                          // 700,
                        ),
                        const SizedBox(
                          width: 24,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 27 + 27,
                              child: Column(children: const [
                                Text('รหัสผู้ให้บริการ:'),
                                Text('NX23418')
                              ]),
                            ),
                            const SizedBox(
                              height: 35,
                            ),
                            SizedBox(
                              height: 27 + 27,
                              child: Column(children: const [
                                Text('Logo ของผู้ให้บริการ'),
                                Text('(512 x 512 px)')
                              ]),
                            ),
                            const SizedBox(
                              height: 35,
                            ),
                            Container(
                                width: 126,
                                height: 42,
                                decoration: BoxDecoration(
                                  color: const Color(0xfff4c700),
                                  borderRadius: BorderRadius.circular(8),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                ))
                          ],
                        )
                      ],
                    ),
                  ),
                  //!<---ZoneB01->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    //  color: const Color(0xff7b7b7b),
                    height: 340,
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          // height: 37,
                          child: const Text("สถานที่ตั้ง",
                              style: TextStyle(
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                        ),
                        Container(
                          // width: 380,
                          height: 265,
                          decoration: BoxDecoration(
                              color: const Color(0xffffffff),
                              borderRadius: BorderRadius.circular(8)),
                          child: Row(children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                //  / color: Colors.purple,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 20),
                                child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        //,
                                        children: [
                                          Container(
                                              // color: Colors.green,
                                              width: 129,
                                              height: 40,
                                              alignment: Alignment.topRight,
                                              padding: const EdgeInsets.only(
                                                  right: 37, top: 0),
                                              child: const Text("ตั้งอยู่ที่")),
                                          // SizedBox(
                                          //   width: 37,
                                          // ),
                                          const Expanded(
                                            child: Text(
                                              "สำนักงานใหญ่ 125 ซอยแก้วเพชร",
                                              textAlign: TextAlign.start,
                                            ),
                                          )
                                        ],
                                      ),
                                      Row(
                                        //mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                              width: 129,
                                              alignment: Alignment.topRight,
                                              padding: const EdgeInsets.only(
                                                  right: 37),
                                              child: Text("รหัสไปรษณีย์")),
                                          const Text(
                                            "10800",
                                            textAlign: TextAlign.end,
                                          )
                                        ],
                                      ),
                                      Row(
                                        //mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                              width: 129,
                                              alignment: Alignment.topRight,
                                              padding:
                                                  EdgeInsets.only(right: 37),
                                              child: Text("จังหวัด")),
                                          Text(
                                            "กรุงเทพมหานคร",
                                            textAlign: TextAlign.end,
                                          )
                                        ],
                                      ),
                                      Row(
                                        //mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                              width: 129,
                                              alignment: Alignment.topRight,
                                              padding: const EdgeInsets.only(
                                                  right: 37),
                                              child: const Text("เขต/อำเภอ")),
                                          const Text(
                                            "บางซื่อ",
                                            textAlign: TextAlign.end,
                                          )
                                        ],
                                      ),
                                      Row(
                                        //mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                              width: 129,
                                              alignment: Alignment.topRight,
                                              padding: const EdgeInsets.only(
                                                  right: 37),
                                              child: const Text("แขวง/ตำบล")),
                                          const Text(
                                            "วงศ์สว่าง",
                                            textAlign: TextAlign.end,
                                          )
                                        ],
                                      ),
                                    ]),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.topRight,
                                //color: Colors.amber,
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: 42,
                                  height: 42,
                                  decoration: BoxDecoration(
                                    color: const Color(0xfff4c700),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ]),
                        )
                      ],
                    ),
                  ),
                  //!<---ZoneB02->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    //    color: const Color(0xff00d792),
                    height: 340,
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8.0),
                          alignment: Alignment.centerLeft,
                          // height: 37,
                          child: const Text("สถานที่ตั้ง",
                              style: TextStyle(
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                        ),
                        Container(
                            // width: 380,
                            height: 265,
                            decoration: BoxDecoration(
                                color: const Color(0xffffffff),
                                borderRadius: BorderRadius.circular(8)),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 18),
                                  child: Container(
                                    height: 100,
                                    //    color: Color(0xffff886c),
                                    child: Stack(children: [
                                      Align(
                                        child: Container(
                                            // width: 364,
                                            height: 58,
                                            decoration: BoxDecoration(
                                                color: const Color(0xffffea8a),
                                                borderRadius:
                                                    BorderRadius.circular(4))),
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                            width: 96,
                                            height: 96,
                                            decoration: BoxDecoration(
                                              color: const Color(0xffc2e82c),
                                            )),
                                      )
                                    ]),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Container(
                                    //  color: Color(0xff045cc9),
                                    height: 100,
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Row(
                                            //mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  width: 129,
                                                  alignment: Alignment.topRight,
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 37),
                                                  child: Text("ชื่อ-นามสกุล")),
                                              const Text(
                                                "สุกิจ ศุภรัตน์ภิญโญ",
                                                textAlign: TextAlign.end,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                          Row(
                                            //mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  width: 129,
                                                  alignment: Alignment.topRight,
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 37),
                                                  child: Text("อีเมลติดต่อ")),
                                              const Text(
                                                "nicenoces@gmail.com",
                                                textAlign: TextAlign.end,
                                                overflow: TextOverflow.ellipsis,
                                              )
                                            ],
                                          ),
                                          Row(
                                            //mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  width: 129,
                                                  alignment: Alignment.topRight,
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 37),
                                                  child: Text("เบอร์ติดต่อ")),
                                              const Text(
                                                "0628899942",
                                                textAlign: TextAlign.end,
                                              )
                                            ],
                                          ),
                                        ]),
                                  ),
                                )
                              ],
                            ))
                      ],
                    ), // 700,
                  ),
                ]),
              ),
            ),
            //*CenterMid<--ZoneB1-->
            Expanded(
              child: Container(
                //  color: Colors.green,
                height: heightCollum_ZoneB,
                child: Column(children: [
                  //!<---ZoneB10->
                  Container(
                      padding: const EdgeInsets.all(8.0),
                      alignment: Alignment.centerLeft,
                      //   color: const Color(0xffd15188),
                      height: 270,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Nicety Nine Co., Ltd.",
                                style: TextStyle(
                                  color: Color(0xff000000),
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(
                              height: 5,
                            ),
                            const Text("เลขประจำตัวผู้เสียภาษี: 0105559195145",
                                style: TextStyle(
                                  color: Color(0xff000000),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              // width: 380,
                              height: 175,
                              decoration: BoxDecoration(
                                  color: const Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Center(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 17, horizontal: 8),
                                        child: const Text(
                                            "Nicetynine is software developers for everyone. We aim to create the product that will help your life easiler and happiler.\nFor more info: https://nicetynine.com",
                                            style: TextStyle(
                                              // fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff000000),
                                              // fontSize: 18,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              letterSpacing: 0,
                                            )),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomRight,
                                      child: Container(
                                          margin: const EdgeInsets.only(
                                              right: 8, bottom: 8),
                                          width: 42,
                                          height: 42,
                                          decoration: BoxDecoration(
                                            color: Color(0xfff4c700),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color(0x3f000000),
                                                  offset: Offset(0, 1),
                                                  blurRadius: 2,
                                                  spreadRadius: 0)
                                            ],
                                          )),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ])),
                  //!<---ZoneB11->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    //  color: const Color(0xff55e8c1),
                    height: 680,
                    child: Column(children: [
                      Container(
                        height: 50,
                        alignment: Alignment.centerLeft,
                        child: const Text("ตำแหน่งที่ตั้ง",
                            style: TextStyle(
                              color: Color(0xff000000),
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            )),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        // width: 380,
                        height: 600,
                        decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(8)),
                      )
                    ]),
                  ),
                ]),
              ),
            ),
            //*CenterRight<--ZoneB2-->
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                //   color: Colors.red,
                height: heightCollum_ZoneB,
                child: Column(children: [
                  //!CenterRight<--ZoneB20-->
                  Container(
                    //padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.bottomCenter,
                    color: Color(0xfff4c700),
                    height: 253,
                    child: Container(
                        width: 181,
                        height: 177,
                        decoration: const BoxDecoration(
                          color: Color(0xffffffff),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0, 2),
                                blurRadius: 10,
                                spreadRadius: 0)
                          ],
                        )),
                  ),
                  //!CenterRight<--ZoneB21-->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    //  color: const Color(0xffd15188),
                    height: 300,
                    child: Column(children: [
                      Container(
                          height: 50,
                          alignment: Alignment.centerLeft,
                          child: const Text("เอกสารสรุปรายได้",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ))),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 177,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 10,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: Column(
                            children: [
                              Container(
                                  // width: 354,
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 14, vertical: 13),
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: const Color(0xff93162d),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  )),
                            ],
                          ),
                        ),
                      )
                    ]),
                  ),
                  //!CenterRight<--ZoneB22-->
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.centerLeft,
                    //  color: const Color(0xff7f7f7f),
                    height: 300,
                    child: Column(children: [
                      Container(
                          height: 50,
                          alignment: Alignment.centerLeft,
                          child: const Text("สัญญาที่ทำร่วมกับ nt",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff000000),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ))),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 177,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 10,
                                  spreadRadius: 0)
                            ],
                          ),
                        ),
                      )
                    ]),
                  ),
                ]),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}









// back up code 

// class Profile extends StatelessWidget {
//   const Profile({super.key});

//   @override
//   Widget build(BuildContext context) {
//     const double heightCollum_ZoneB = 700.0;
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Center(
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               //TOP Zone<---ZoneA-
//               // Container(
//               //   alignment: Alignment.centerLeft,
//               //   child: const Text(
//               //     'ลงทะเบียนเป็นผู้ให้บริการ',
//               //     style: TextStyle(fontSize: 24),
//               //   ),
//               // ),
//               //Center ZONE<------
//               SizedBox(
//                 child: Row(children: [
//                   Expanded(
//                     //*CenterLeft<---ZoneB0->
//                     child: Container(
//                       color: Colors.pink,
//                       height: heightCollum_ZoneB, //700,
//                     ),
//                   ),
//                   //*CenterMid<--ZoneB1-->
//                   Expanded(
//                     child: Container(
//                       color: Colors.green,
//                       height: heightCollum_ZoneB, // 700,
//                     ),
//                   ),
//                   //*CenterRight<--ZoneB2-->
//                   Expanded(
//                     child: Container(
//                       alignment: Alignment.centerLeft,
//                       color: Colors.red,
//                       height: heightCollum_ZoneB, // 700,
//                     ),
//                   ),
//                 ]),
//               ),
//               //Bottom ZONE<--- ZoneC----
//               // const SizedBox(
//               //   height: 20,
//               // ),
//               // Container(
//               //   alignment: Alignment.centerRight,
//               //   height: 54,
//               //   child: Container(width: 244, color: Colors.amber),
//               // ),
//               // Container(
//               //   child: const Text(
//               //     '© บริษัท โทรคมนาคมแห่งชาติ (มหาชน) จำกัด',
//               //     style: TextStyle(fontSize: 16),
//               //   ),
//               // ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }


