import 'package:flutter/material.dart';

class PopUp_ContractSelection extends StatelessWidget {
  const PopUp_ContractSelection({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 100,
        width: 100,
        color: Colors.red,
        child: TextButton(
          child: const Text("PopUp_ProfileEdit"),
          onPressed: () {
            popUp_ContractSelection(context);
          },
        ),
      ),
    );
  }

  popUp_ContractSelection(BuildContext context) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              backgroundColor: Colors.transparent,
              // title: Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              //     Container(
              //         width: 153.9999999999999,
              //         height: 47,
              //         decoration: const BoxDecoration(color: Colors.pink)),
              //     Container(
              //         width: 42,
              //         height: 42,
              //         decoration: const BoxDecoration(color: Colors.blue))
              //   ],
              // ),
              content: Container(
                // color: Colors.pink,
                width: 447,
                padding: EdgeInsets.all(8),
                child: popUp_ContractSelection_Content(),
                //   child: const Text('AlertDialog description')),
                // actions: <Widget>[
                //   TextButton(
                //     onPressed: () => Navigator.pop(context, 'Cancel'),
                //     child: const Text('Cancel'),
                //   ),
                //   TextButton(
                //     onPressed: () => Navigator.pop(context, 'OK'),
                //     child: const Text('OK'),
                //   ),
                // ],
              ),
            ));
  }
}

class popUp_ContractSelection_Content extends StatelessWidget {
  const popUp_ContractSelection_Content({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(height: 50, width: 50, color: Colors.pink),
        const SizedBox(
          height: 22,
        ),
        Container(
          color: Colors.pink,
          //   height: 615,
          //  margin: EdgeInsets.all(10),
          child: Column(children: [
            const Text("สัญญาที่ทำร่วมกับ nt",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet',
                  color: Color(0xff000000),
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                )),
            Container(
                // width: 380,
                padding: const EdgeInsets.all(10),
                height: 400,
                decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                    borderRadius: BorderRadius.circular(8))),
            const SizedBox(
              height: 24.0,
            ),
            Container(
                //width: 383,
                height: 40,
                decoration: BoxDecoration(
                  color: const Color(0xfff4c700),
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: const [
                    BoxShadow(
                        color: Color(0x3f000000),
                        offset: Offset(0, 1),
                        blurRadius: 2,
                        spreadRadius: 0)
                  ],
                ))
          ]),
        )
      ],
    );
  }
}
