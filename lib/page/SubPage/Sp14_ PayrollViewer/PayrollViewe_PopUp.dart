import 'package:flutter/material.dart';

class PopUpPayrollView extends StatelessWidget {
  const PopUpPayrollView({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 100,
        width: 100,
        color: Colors.red,
        child: TextButton(
          child: const Text("PopUp_ProfileEdit"),
          onPressed: () {
            popUpPayrollView(context);
          },
        ),
      ),
    );
  }

  Future<String?> popUpPayrollView(BuildContext context) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              // title: Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              //     Container(
              //         width: 153.9999999999999,
              //         height: 47,
              //         decoration: const BoxDecoration(color: Colors.pink)),
              //     Container(
              //         width: 42,
              //         height: 42,
              //         decoration: const BoxDecoration(color: Colors.blue))
              //   ],
              // ),
              backgroundColor: Colors.transparent,
              content: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  //  padding: EdgeInsets.all(120),
                  child: const PopUpPayrollView_Content(),
                  //   child: const Text('AlertDialog description')),
                  // actions: <Widget>[
                  //   TextButton(
                  //     onPressed: () => Navigator.pop(context, 'Cancel'),
                  //     child: const Text('Cancel'),
                  //   ),
                  //   TextButton(
                  //     onPressed: () => Navigator.pop(context, 'OK'),
                  //     child: const Text('OK'),
                  //   ),
                  // ],
                ),
              ),
            ));
  }
}

class PopUpPayrollView_Content extends StatelessWidget {
  const PopUpPayrollView_Content({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 50, width: 50,
              // margin: EdgeInsets.all(50.0),
              decoration:
                  BoxDecoration(color: Colors.blue, shape: BoxShape.circle),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: 697,
              height: 790,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.circular(24),
                boxShadow: const [
                  BoxShadow(
                      color: Color(0x7f000000),
                      offset: Offset(0, 2),
                      blurRadius: 4,
                      spreadRadius: 0)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
