import 'package:flutter/material.dart';

class BankAccount_subP extends StatelessWidget {
  const BankAccount_subP({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Container(
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: Container(
                  height: 936,
                  // color: Colors.amber,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                            alignment: Alignment.center,
                            height: 80,
                            decoration: BoxDecoration(
                                color: const Color(0xfff4c700),
                                borderRadius: BorderRadius.circular(18)),
                            child: Row(
                              children: [
                                const SizedBox(width: 31),
                                Container(
                                  width: 41,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                const SizedBox(width: 20),
                                const Text("Book Bank Detail",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff000000),
                                      fontSize: 24,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    ))
                              ],
                            )),
                        const SizedBox(
                          height: 18,
                        ),
                        Container(
                            width: 526,
                            height: 500,
                            decoration: BoxDecoration(
                                color: Color(0xfff4c700),
                                borderRadius: BorderRadius.circular(24))),
                        const SizedBox(
                          height: 36,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 260,
                          height: 42,
                          decoration: BoxDecoration(
                            color: const Color(0xfff4c700),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 1),
                                  blurRadius: 2,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: const Text("ยืนยันข้อมูลถูกต้อง",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff000000),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                        ),
                        const SizedBox(
                          height: 80,
                        ),
                        Container(
                          // height: 120,

                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: const Color(0xfff4c700),
                            borderRadius: BorderRadius.circular(18),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("หมายเหตุ:",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xff000000),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  )),
                              Expanded(
                                child: Text(
                                    " 1. ผู้ให้บริการจะต้องระบุเลขที่บัญชีธนาคารให้ถูกต้อง บริษัทฯ ไม่รับผิดชอบในกรณีโอนเงินผิดพลาดโดยมีสาเหตุจากข้อมูลเลขที่บัญชีธนาคารที่บัญชีไว้ไม่ถูกต้อง\n2. ระบบจะคำนวณรายได้เมื่อลูกค้าทำการ Redeem Voucher สำเร็จ หรือ Campaign ได้หมดอายุลงก่อนลูกค้า ทำการ Redeem Voucher สำเร็จ สรุปยอดทุกสิ้นเดือน โดยเงินจะโอนเข้าบัญชีของท่านในกลางเดือนถัดไป",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff000000),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              //Right Zone
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 380,
                  height: 936,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(8)),
                  child: Column(
                    children: [
                      const Text("Book Banks Profile",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff000000),
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      const SizedBox(
                        height: 18,
                      ),
                      Container(
                          width: 128,
                          height: 128,
                          decoration: const BoxDecoration(
                            color: Colors.pink,
                          )),
                      const SizedBox(
                        height: 36,
                      ),
                      Container(
                          width: 181,
                          height: 181,
                          decoration: BoxDecoration(
                            color: const Color(0xfff4c700),
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 2,
                                  spreadRadius: 0)
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
