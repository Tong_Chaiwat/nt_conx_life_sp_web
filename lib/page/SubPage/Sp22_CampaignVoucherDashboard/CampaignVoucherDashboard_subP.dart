import 'package:flutter/material.dart';

class CampaignVoucherDashboard_subP extends StatelessWidget {
  const CampaignVoucherDashboard_subP({super.key});
  @override
  Widget build(BuildContext context) {
    double _height = 835;
    return Center(
      child: SingleChildScrollView(
        child: Container(
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: Container(
                  height: 936,
                  // color: Colors.amber,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 80,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 15,
                          ),
                          decoration: BoxDecoration(
                              color: const Color(0xfff4c700),
                              borderRadius: BorderRadius.circular(18)),
                          child: Row(
                            children: [
                              SizedBox(
                                child: Row(children: [
                                  const Text("Campaigns",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet',
                                        color: Color(0xff000000),
                                        fontSize: 24,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  const SizedBox(
                                    width: 21,
                                  ),
                                  Container(
                                      width: 41,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          color: Color(0xffffffff),
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  const Text("อัพเดทล่าสุด \n01/11/2022 09:15",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff000000),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ))
                                ]),
                              ),
                              const SizedBox(
                                width: 50,
                              ),
                              SizedBox(
                                child: Row(
                                  children: [
                                    Container(
                                        width: 41,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: Color(0xfff7f7f7),
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    SizedBox(
                                      width: 23,
                                    ),
                                    Container(
                                      width: 84,
                                      height: 42,
                                      decoration: BoxDecoration(
                                          color: Color(0xffffffff),
                                          borderRadius:
                                              BorderRadius.circular(21)),
                                    ),
                                    SizedBox(
                                      width: 23,
                                    ),
                                    Container(
                                      width: 84,
                                      height: 42,
                                      decoration: BoxDecoration(
                                          color: Color(0xffffffff),
                                          borderRadius:
                                              BorderRadius.circular(21)),
                                    ),
                                    SizedBox(
                                      width: 23,
                                    ),
                                    Container(
                                      width: 84,
                                      height: 42,
                                      decoration: BoxDecoration(
                                          color: Color(0xffffffff),
                                          borderRadius:
                                              BorderRadius.circular(21)),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Container(
                          alignment: Alignment.center,
                          height: 832,
                          decoration: BoxDecoration(
                              color: const Color(0xffffffff),
                              borderRadius: BorderRadius.circular(18)),
                          child: Wrap(
                            spacing: 10,
                            runSpacing: 5,
                            children: [
                              for (var i = 0; i < 10; i++)
                                Container(
                                    width: 181,
                                    height: 232,
                                    decoration: BoxDecoration(
                                      //  color: Color(0xffffffff),
                                      color: Colors.pink,
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 2),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ))
                            ],
                          ),
                        ),
                        // Container(
                        //   // height: 120,

                        //   padding: EdgeInsets.all(10),
                        //   decoration: BoxDecoration(
                        //     color: const Color(0xfff4c700),
                        //     borderRadius: BorderRadius.circular(18),
                        //   ),
                        //   child: Row(
                        //     crossAxisAlignment: CrossAxisAlignment.start,
                        //     children: const [
                        //       Text("หมายเหตุ:",
                        //           style: TextStyle(
                        //             fontFamily: 'SukhumvitSet-Text',
                        //             color: Color(0xff000000),
                        //             fontSize: 16,
                        //             fontWeight: FontWeight.w400,
                        //             fontStyle: FontStyle.normal,
                        //             letterSpacing: 0,
                        //           )),
                        //       Expanded(
                        //         child: Text(
                        //             " 1. ผู้ให้บริการจะต้องระบุเลขที่บัญชีธนาคารให้ถูกต้อง บริษัทฯ ไม่รับผิดชอบในกรณีโอนเงินผิดพลาดโดยมีสาเหตุจากข้อมูลเลขที่บัญชีธนาคารที่บัญชีไว้ไม่ถูกต้อง\n2. ระบบจะคำนวณรายได้เมื่อลูกค้าทำการ Redeem Voucher สำเร็จ หรือ Campaign ได้หมดอายุลงก่อนลูกค้า ทำการ Redeem Voucher สำเร็จ สรุปยอดทุกสิ้นเดือน โดยเงินจะโอนเข้าบัญชีของท่านในกลางเดือนถัดไป",
                        //             style: TextStyle(
                        //               fontFamily: 'SukhumvitSet-Text',
                        //               color: Color(0xff000000),
                        //               fontSize: 16,
                        //               fontWeight: FontWeight.w400,
                        //               fontStyle: FontStyle.normal,
                        //               letterSpacing: 0,
                        //             )),
                        //       ),
                        //     ],
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ),
              //Right Zone
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 380,
                  height: _height,
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  margin: const EdgeInsets.only(bottom: 100),
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(8)),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Voucher Redemption",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff000000),
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                        ),
                        const SizedBox(
                          height: 26,
                        ),
                        Container(
                          width: 181,
                          height: 180,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 2,
                                  spreadRadius: 0)
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 26,
                        ),
                        const Text(
                          "เพื่อให้ได้ประสบการณ์ที่ดีกว่า\nเราขอแนะนำให้ท่านใช้งาน Voucher Redemption \nผ่านทางโทรศัพท์มือถือของท่าน",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff000000),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(
                          height: 32,
                        ),
                        Container(
                            width: 380,
                            height: 120,
                            decoration:
                                BoxDecoration(color: Color(0xff09b4e3))),
                        Container(
                            width: 380,
                            height: 120,
                            decoration:
                                BoxDecoration(color: Color(0xffb33928))),
                        Container(
                            width: 380,
                            height: 120,
                            decoration:
                                BoxDecoration(color: Color(0xff14b181))),
                        const SizedBox(
                          height: 13,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                width: 42,
                                height: 42,
                                decoration: BoxDecoration(
                                  color: Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(8),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                            SizedBox(
                              width: 17,
                            ),
                            Text("อัพเดทล่าสุด 01/11/2022 09:15",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff000000),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                ))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
