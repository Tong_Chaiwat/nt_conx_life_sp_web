import 'package:flutter/material.dart';

class ContractViewer extends StatelessWidget {
  const ContractViewer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      // color: Colors.pink,
      child: Center(
          child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: 42,
              height: 42,
              decoration: BoxDecoration(
                color: Color(0xfff4c700),
                borderRadius: BorderRadius.circular(8),
                boxShadow: const [
                  BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0, 1),
                      blurRadius: 2,
                      spreadRadius: 0)
                ],
              )),
          const SizedBox(
            width: 51,
          ),
          Center(
            child: Container(
              child: SingleChildScrollView(
                  child: Container(
                padding: const EdgeInsets.only(left: 20, right: 120),
                child: Column(children: [
                  Container(
                      width: 800,
                      height: 950,
                      decoration: BoxDecoration(color: Colors.green)),
                  Container(
                      width: 800,
                      height: 950,
                      decoration: BoxDecoration(color: Colors.black))
                ]),
              )),
            ),
          )
        ],
      )),
    );
  }
}
