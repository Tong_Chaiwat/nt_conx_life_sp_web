import 'package:flutter/material.dart';

class SP8_Profile_registration_nofill extends StatelessWidget {
  const SP8_Profile_registration_nofill({super.key});

  @override
  Widget build(BuildContext context) {
    const double HeightCollum = 700.0;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              //TOP Zone<----
              Container(
                alignment: Alignment.centerLeft,
                child: const Text(
                  'ลงทะเบียนเป็นผู้ให้บริการ',
                  style: TextStyle(fontSize: 24),
                ),
              ),
              //Center ZONE<------
              Container(
                child: Row(children: [
                  Expanded(
                    //*CenterLeft<---->
                    child: Container(
                      color: Colors.transparent,
                      height: HeightCollum, //700,
                      child: Center(
                          child: Column(children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            'ข้อมูลผู้ดูแลหลัก',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ),
                        const SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.all(13.0),
                          child: Card(
                            child: Row(children: [
                              Container(
                                width: 83,
                                height: 83,
                                decoration: BoxDecoration(
                                  color: const Color(0xff070504),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    constraints: const BoxConstraints(
                                        minWidth: 100, maxWidth: 200),
                                    child: const Text(
                                      "รูปโปรไฟล์ของผู้ใช้งานหลัก",
                                      style: TextStyle(
                                        color: Color(0xff000000),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                          width: 126,
                                          height: 42,
                                          decoration: BoxDecoration(
                                            color: const Color(0xfff4c700),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color(0x3f000000),
                                                  offset: Offset(0, 1),
                                                  blurRadius: 2,
                                                  spreadRadius: 0)
                                            ],
                                          )),
                                      Container(
                                        margin: const EdgeInsets.only(
                                            left: 3, top: 20),
                                        child: const Text("(512 x 512 px)",
                                            style: TextStyle(
                                              color: Color(0xff808080),
                                              fontSize: 16,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              letterSpacing: 0,
                                            )),
                                      )
                                    ],
                                  ),
                                ],
                              )
                            ]),
                          ),
                        ),
                        const SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("อีเมล*"),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    // color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("รหัสผ่าน*"),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    // color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("ยืนยันรหัสผ่าน*"),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    //  color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            'ข้อมูลส่วนบุคคล',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ),
                        const SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("ชื่อ*"),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    //  color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("นามสกุล*"),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    //  color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, bottom: 24),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text("เลขบัตรประชาชน*\n/Passport",
                                  textAlign: TextAlign.right),
                              const SizedBox(width: 5),
                              Container(
                                  width: 228,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    //  color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x3f000000),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                          spreadRadius: 0)
                                    ],
                                  ))
                            ],
                          ),
                        ),
                      ])),
                    ),
                  ),
                  //*CenterMid<---->
                  Expanded(
                    child: Container(
                      //color: Colors.green,
                      height: HeightCollum, // 700,
                      child: Center(
                        child: Column(children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'ข้อมูลผู้ให้บริการ',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          ),
                          const SizedBox(height: 40),
                          Padding(
                            padding: const EdgeInsets.all(13.0),
                            child: Card(
                              child: Row(children: [
                                Container(
                                  width: 83,
                                  height: 83,
                                  decoration: BoxDecoration(
                                    color: const Color(0xff070504),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 100, maxWidth: 200),
                                      child: const Text(
                                        "Logo ของผู้ให้บริการ",
                                        style: TextStyle(
                                          color: Color(0xff000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                            width: 126,
                                            height: 42,
                                            decoration: BoxDecoration(
                                              color: const Color(0xfff4c700),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color(0x3f000000),
                                                    offset: Offset(0, 1),
                                                    blurRadius: 2,
                                                    spreadRadius: 0)
                                              ],
                                            )),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              left: 3, top: 20),
                                          child: const Text("(512 x 512 px)",
                                              style: TextStyle(
                                                color: Color(0xff808080),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 0,
                                              )),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              ]),
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("ชื่อผู้ให้บริการ*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 228,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      // color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("ตั้งอยู่ที่*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 228,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      //  color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("รหัสไปรษณีย์*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 107,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      //  color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                                const SizedBox(width: 5),
                                Container(
                                    width: 101,
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff4c700),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("จังหวัด*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 228,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      //  color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("เขต/อำเภอ*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 228,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      //  color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 10, bottom: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text("แขวง/ตำบล*"),
                                const SizedBox(width: 5),
                                Container(
                                    width: 228,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      //  color: const Color(0xffffffff),
                                      borderRadius: BorderRadius.circular(4),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 10, left: 10),
                            child: Container(
                                height: 42,
                                decoration: BoxDecoration(
                                  color: const Color(0xffdbdcdd),
                                  borderRadius: BorderRadius.circular(8),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x3f000000),
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0)
                                  ],
                                )),
                          )
                        ]),
                      ),
                    ),
                  ),
                  //*CenterRight<---->
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      color: Colors.red,
                      height: HeightCollum, // 700,
                      child: Padding(
                        padding: const EdgeInsets.all(50.0),
                        child: Center(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  "เลขประจำตัวผู้เสียภาษี (ถ้ามี)",
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 0, left: 0),
                                child: Container(
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffdbdcdd),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  'นโยบายและเงื่อนไขการใช้งาน',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              const SizedBox(
                                height: 28,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 203,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          color: const Color(0xfff4c700),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius:
                                              BorderRadius.circular(10)
                                          //more than 50% of width makes circle
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 28,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 203,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          color: const Color(0xfff4c700),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius:
                                              BorderRadius.circular(10)
                                          //more than 50% of width makes circle
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  "เบอร์โทรศัพท์ที่รับ SMS ได้*",
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 0, left: 0),
                                child: Container(
                                    height: 42,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffdbdcdd),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Color(0x3f000000),
                                            offset: Offset(0, 1),
                                            blurRadius: 2,
                                            spreadRadius: 0)
                                      ],
                                    )),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                      width: 126,
                                      height: 42,
                                      decoration: BoxDecoration(
                                        color: const Color(0xffdbdcdd),
                                        borderRadius: BorderRadius.circular(8),
                                        boxShadow: const [
                                          BoxShadow(
                                              color: Color(0x3f000000),
                                              offset: Offset(0, 1),
                                              blurRadius: 2,
                                              spreadRadius: 0)
                                        ],
                                      ))),
                              const SizedBox(
                                height: 20,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  "รหัส OTP*",
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Container(
                                      width: 128,
                                      height: 40,
                                      decoration: BoxDecoration(
                                        color: const Color(0xffdbdcdd),
                                        borderRadius: BorderRadius.circular(4),
                                        boxShadow: const [
                                          BoxShadow(
                                              color: Color(0x3f000000),
                                              offset: Offset(0, 1),
                                              blurRadius: 2,
                                              spreadRadius: 0)
                                        ],
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      width: 92,
                                      height: 42,
                                      decoration: BoxDecoration(
                                        color: const Color(0xffdbdcdd),
                                        borderRadius: BorderRadius.circular(8),
                                        boxShadow: const [
                                          BoxShadow(
                                              color: Color(0x3f000000),
                                              offset: Offset(0, 1),
                                              blurRadius: 2,
                                              spreadRadius: 0)
                                        ],
                                      ))
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
              //Bottom ZONE<-------
              const SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.centerRight,
                height: 54,
                child: Container(width: 244, color: Colors.amber),
              ),
              Container(
                child: const Text(
                  '© บริษัท โทรคมนาคมแห่งชาติ (มหาชน) จำกัด',
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
