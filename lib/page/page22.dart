import 'package:flutter/material.dart';

import 'P22CampaignVoucherDashboard/CampaignVoucherDashboard.dart';

class Page22 extends StatelessWidget {
  const Page22({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page22Body();
  }
}

class Page22Body extends StatelessWidget {
  const Page22Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignVoucherDashboard();
  }
}
