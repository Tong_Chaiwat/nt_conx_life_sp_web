import 'package:flutter/material.dart';

import 'P18ActivitiesCustomers/ActivitiesCustomers.dart';

class Page18 extends StatelessWidget {
  const Page18({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page18Body();
  }
}

class Page18Body extends StatelessWidget {
  const Page18Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ActivitiesCustomers();
  }
}
