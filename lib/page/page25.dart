import 'package:flutter/material.dart';

import 'P25CampaignBuilderEdit/CampaignBuilderEdit.dart';

class Page25 extends StatelessWidget {
  const Page25({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page25Body();
  }
}

class Page25Body extends StatelessWidget {
  const Page25Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignBuilderEdit();
  }
}
