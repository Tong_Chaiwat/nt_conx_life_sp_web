import 'package:flutter/material.dart';

import 'P8Profile_registration/P8Profile_registration_nofill.dart';

class Page8 extends StatelessWidget {
  const Page8({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page8Body();
  }
}

class Page8Body extends StatelessWidget {
  const Page8Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Profile_registration_nofill();
  }
}
