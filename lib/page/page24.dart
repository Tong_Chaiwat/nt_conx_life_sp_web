import 'package:flutter/material.dart';

import 'P24CampaignBuilderFill/CampaignBuilderFill.dart';

class Page24 extends StatelessWidget {
  const Page24({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page24Body();
  }
}

class Page24Body extends StatelessWidget {
  const Page24Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignBuilderFill();
  }
}
