import 'package:flutter/material.dart';

import 'P26CampaignActive-Edit-DetailTop/CampaignActive_Edit_DetailTop.dart';

class Page26 extends StatelessWidget {
  const Page26({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page26Body();
  }
}

class Page26Body extends StatelessWidget {
  const Page26Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignActive_Edit_DetailTop();
  }
}
