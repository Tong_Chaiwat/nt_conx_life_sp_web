import 'package:flutter/material.dart';

import '../SubPage/Sp12_ContractSelection/PopUp_ContractSelection.dart';

class ContractSelection extends StatelessWidget {
  const ContractSelection({super.key});

  @override
  Widget build(BuildContext context) {
    return PopUp_ContractSelection();
  }
}
