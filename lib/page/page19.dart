import 'package:flutter/material.dart';

import 'P19ActivitiesStaffs/ActivitiesStaffs.dart';

class Page19 extends StatelessWidget {
  const Page19({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page19Body();
  }
}

class Page19Body extends StatelessWidget {
  const Page19Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ActivitiesStaffs();
  }
}
