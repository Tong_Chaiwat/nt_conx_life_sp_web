import 'package:flutter/material.dart';

import 'P21ServicesChart/ServicesChart.dart';

class Page21 extends StatelessWidget {
  const Page21({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page21Body();
  }
}

class Page21Body extends StatelessWidget {
  const Page21Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ServicesChart();
  }
}
