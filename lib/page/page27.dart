import 'package:flutter/material.dart';

import 'P27CampaignActive_Edit_DetailBottom/CampaignActive_Edit_DetailBottom.dart';

class Page27 extends StatelessWidget {
  const Page27({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page27Body();
  }
}

class Page27Body extends StatelessWidget {
  const Page27Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignActive_Edit_DetailBottom();
  }
}
