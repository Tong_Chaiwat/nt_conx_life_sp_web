import 'package:flutter/material.dart';

import 'P23CampaignBuilderNew /CampaignBuilderNew.dart';

class Page23 extends StatelessWidget {
  const Page23({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page23Body();
  }
}

class Page23Body extends StatelessWidget {
  const Page23Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CampaignBuilderNew();
  }
}
