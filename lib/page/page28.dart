import 'package:flutter/material.dart';

import 'P28Campaign-BenefitSelection/Campaign_BenefitSelection.dart';

class Page28 extends StatelessWidget {
  const Page28({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page28Body();
  }
}

class Page28Body extends StatelessWidget {
  const Page28Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Campaign_BenefitSelection();
  }
}
