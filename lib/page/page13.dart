import 'package:flutter/material.dart';

import 'P13ContractViewer/ContractViewe_read.dart';

class Page13 extends StatelessWidget {
  const Page13({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Page13Body();
  }
}

class Page13Body extends StatelessWidget {
  const Page13Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContractViewe_read();
    // return Center(
    //     child: Container(
    //       height: 100,
    //       width: 100,
    //       color: Colors.blue,
    //       child: const Text("PAGE 1"),
    //     ),
    //   );
  }
}
